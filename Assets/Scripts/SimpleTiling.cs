﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTiling : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        //Set tiling to X and Y (both * 10).
        GetComponent<Renderer>().material.mainTextureScale = new Vector2(transform.localScale.x * 10, transform.localScale.z * 10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
