﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Move character by Horizontal and Vertical axis (binary 0/1 thanks to GetAxisRaw).
        //Since the GetAxisRaw returns 0 if it's not recieving input, this works just fine.
        transform.Translate(Input.GetAxisRaw("Horizontal") / 12, 0, Input.GetAxisRaw("Vertical") / 12);
    }
}
