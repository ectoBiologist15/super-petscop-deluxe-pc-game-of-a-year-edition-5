﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TextBox : MonoBehaviour
{

    public Renderer InteractPrompt;
    public string[] text;
    int currentletter;
    int currentstring;
    string currenttext;
    string fulltext;
    GameObject player;
    bool textboxopen;
    Image tbox;
    Text texttext;

    // Start is called before the first frame update
    void Start()
    {
        //set shit idk
        player = GameObject.Find("Player");
        currentletter = 0;
        InteractPrompt = GameObject.Find(this.gameObject.name + "Interact").GetComponent<Renderer>();
        InteractPrompt.enabled = false;
        tbox = GameObject.Find("TextBox").GetComponent<Image>();
        texttext = GameObject.Find("TextBoxText").GetComponent<Text>();
        tbox.enabled = false;
        texttext.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Z stuff
        //Open textbox
        if (Input.GetKeyDown(KeyCode.Z) && InteractPrompt.enabled && !textboxopen)
        {
            textboxopen = true;
            currentletter = 0;
            currentstring = 0;
            fulltext = text[currentstring];
            tbox.enabled = true;
            texttext.enabled = true;
        }
        //Close/Continue textbox
        if (Input.GetKeyDown(KeyCode.Z) && textboxopen && currenttext == fulltext)
        {
            if (currentstring + 1 != text.Length)
            {
                ++currentstring;
                currentletter = 0;
                fulltext = text[currentstring];
                currenttext = "";
            }
            else
            {
                currentstring = 0;
                fulltext = "";
                currenttext = "";
                currentletter = 0;
                tbox.enabled = false;
                texttext.enabled = false;
                textboxopen = false;
            }
        }
        //Skip textbox
        if (Input.GetKeyDown(KeyCode.Z) && textboxopen && currenttext != fulltext)
        {
            currenttext = fulltext;
            currentletter = fulltext.Length;
        }
        //Text box stuff
        if (textboxopen)
        {
            //Set displayed text
            texttext.text = currenttext;
            //Add to text
            if (currenttext != fulltext)
            {
                currenttext = currenttext + fulltext.Substring(currentletter, 1);
                ++currentletter;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            InteractPrompt.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject == player)
        {
            InteractPrompt.enabled = false;
        }
    }
}
